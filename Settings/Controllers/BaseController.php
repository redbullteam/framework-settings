<?php

namespace Modules\Settings\Controllers;

use Modules\Settings\Models\Settings;
use Modules\Framework\Mvc\Controller\Controller;

class BaseController extends Controller {
	protected static function view($name, $vars = array()) {
		parent::view($name, array_merge(Settings::getSiteSettings(), $vars));
	}
}