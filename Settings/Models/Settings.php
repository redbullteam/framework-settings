<?php

namespace Modules\Settings\Models;

use Modules\Framework\Cache\Cache;
use Modules\Framework\Utils\Arrays;

/**
 * @Display("Настройки")
 * @AdminDetailsView("Settings:Admin/settings")
 * @Persist
 */
class Settings extends \Modules\Framework\Mvc\Model\BaseModel {
	/**
	 * @Display("i18n::name")
	 * @Required
	 */
	public $name;
	
	/**
	 * @Debug
	 * @HideFromList
	 * @Display("i18n::type")
	 * @UIHint("select", enum = {"bool", "text", "number", "multilinetext", "html", "password", "upload"})
	 */
	public $type;

	/**
	 * @Display("i18n::value")
	 * @UIHint("custom")
	 */
	public $value;

	private static $instance = null;

	public static function getSiteSettings() {
		if(self::$instance == null) {
			self::$instance = Cache::getInstance()->getOrSet(__CLASS__,
				function () {
					return Arrays::arrayColumn(Settings::LoadCollection(), 'value', 'name');
				},
				time() + 86400);
		}

		return self::$instance;
	}

	public function getFieldType() {
		return $this->type;
	}

	protected function afterSave() {
		if (Cache::getInstance()->contains(__CLASS__))
			Cache::getInstance()->delete(__CLASS__);

		parent::afterSave();
	}
} 