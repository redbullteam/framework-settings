<?php

namespace Modules\Settings;

use Modules\Framework\Core\Config\Config;

class Module extends \Modules\Framework\Core\ModuleSystem\Module {
	public $dependencies = array("Base", "Admin");
	public $autoInstall = true;
	
	public function install() {
		\Modules\Framework\Core\DB::Command()->query(
			sprintf(
				"CREATE TABLE IF NOT EXISTS `%sSettings` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`type` varchar(20) NOT NULL,
					`name` varchar(255) NOT NULL,
					`value` varchar(500) NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE KEY `name` (`name`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1", 
					Config::getInstance()->connection['dbPrefix']));
		
		parent::install();
	}
}