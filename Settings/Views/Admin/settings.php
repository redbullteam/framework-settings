<?php
use Modules\Framework\Globalization\i18n;

/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Utils\Url $__url
 */
$this->extend(\Modules\Framework\Core\Config\Config::getInstance()->admin['layout']);

function getFieldType($field, $model) {
	return $field->uiHint
		? ($field->uiHint == "custom"
			? ($model->type ?: "text")
			: $field->uiHint)
		: ($field->validation && $field->validation->datatype ? $field->validation->datatype : "text");
}
?>
<?php $__section->begin('body'); ?>
	<form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>
				<?=$pageTitle?>
				<small>
					<?php if (!$model->id): ?>
						<?=i18n::current()->newRecord;?>
					<?php else: ?>
						<?=i18n::current()->editRecord;?>
					<?php endif; ?>
				</small>
			</legend>

			<?php foreach ($model->getModelMetadata() as $fieldName => $field): ?>
				<?php if ($field->backend && !$this->backend) continue; ?>
				<?php if ($field->validation && $field->validation->datatype === "hidden" || ($field->debug && !$this->debugEnabled)): ?>
					<?=$this->partial("Base:Shared/Form/Fields/hidden", array('field' => $field, 'model' => $model))?>
					<?php continue; ?>
				<?php endif; ?>
				<div class="control-group <?php if ($field->validation && $field->validation->errorMessage): ?>error<?php endif; ?>">
					<?php if (!$field->validation || ($field->validation->datatype !== "boolean" && $field->validation->datatype !== "radio")):?>
						<label class="control-label" for="<?=$fieldName?>"><?=$field->display?><?php if ($field->validation && $field->validation->required): ?>*<?php endif; ?></label>
					<?php endif; ?>

					<div class="controls">
						<?=$this->partial("Base:Shared/Form/Fields/" . getFieldType($field, $model), array('field' => $field, 'model' => $model))?>

						<?php if ($field->validation && $field->validation->errorMessage): ?>
							<span class="help-inline"><?=$field->validation->errorMessage?></span>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>


			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> <?=i18n::current()->apply;?></button>
				<?php if ($model->id && !$modelPersist): ?>
					<div class="modal hide fade" id="deleteConfirmation">
						<div class="modal-header">
							<button class="close" data-dismiss="modal">×</button>
							<h3><?=i18n::current()->elementRemoving;?></h3>
						</div>
						<div class="modal-body">
							<p><?=i18n::current()->areYouSureYouWantToDeleteItem;?> "<?=(isset($model->title) ? $model->title : $model->id)?>"?</p>
						</div>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn"><?=i18n::current()->cancel;?></a>
							<a href="/admin/<?=$model->getName()?>/delete/<?=$model->id?>" class="btn btn-primary"><?=i18n::current()->ok;?></a>
						</div>
					</div>
					<a href="/admin/<?=$model->getName()?>" data-remote="false" data-toggle="modal" data-target="#deleteConfirmation" class="btn btn-danger"><i class="icon-trash icon-white"></i> <?=i18n::current()->remove;?></a>
				<?php endif; ?>
				<a href="<?=$__url->getUrlBackward()?>" class="btn"><i class="icon-remove"></i> <?=i18n::current()->cancel;?></a>
			</div>
		</fieldset>
	</form>
<?php $__section->end(); ?>